﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ExperisNoroffTask24.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExperisNoroffTask24.Controllers {
    public class HomeController : Controller {

        private static List<Supervisor> supervisors = Supervisor.GetSupervisorSet();

        public IActionResult Index() {

            DateTimeOffset now = DateTimeOffset.Now;

            string currentTimeString = 
                $"{AddZeroToFrontOfNumberIfNeeded(now.Hour)}:" +
                $"{AddZeroToFrontOfNumberIfNeeded(now.Minute)}:" +
                $"{AddZeroToFrontOfNumberIfNeeded(now.Second)}";

            ViewBag.CurrentTime = currentTimeString;

            return View();
        }



        public IActionResult SupervisorOverview() {
            return View("SupervisorOverview", supervisors);
        }


        public IActionResult SupervisorsThatStartsWithS() {
            List<Supervisor> nameStartsWithSList = new List<Supervisor>(
                supervisors.FindAll(s => (s == null) ? false : s.Firstname.Substring(0, 1).ToUpper().Equals("S"))
            );

            return View("SupervisorOverview", nameStartsWithSList);
        }


        /// <summary>
        ///     Routes to a page that show information about
        ///     one Supervisor. Get's an Id from the tag-helper
        ///     asp-route-id
        /// </summary>
        /// <param name="id">Id of the Supervisor to be described</param>
        /// <returns></returns>

        [Route("Home/SupervisorInfo/{id:int}")]
        public IActionResult SupervisorInfo(int id) {

            return View(supervisors.FirstOrDefault(s => (s == null) ? false : s.Id == id));
        }



        [Route("Home/DeleteSupervisor/{id:int}")]
        public IActionResult DeleteSupervisor(int id) {

            foreach(Supervisor s in new List<Supervisor>(supervisors)) {
                if(s?.Id == id) {
                    supervisors.Remove(s);
                }
            }

            return SupervisorOverview();
        }



        public IActionResult SupervisorRegistration() {
            return View();
        }


        
        [HttpPost]
        public ActionResult SupervisorRegistration(Supervisor supervisor) {
            
            if(ModelState.IsValid) {
                supervisors.Add(supervisor);

                return View(supervisor);
            }

            return View();
        }



        /// <summary>
        ///     Forces the number to be two digits long by adding
        ///     a zero to the front of it, if it is less than 10.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string AddZeroToFrontOfNumberIfNeeded(int number) {
            return (number < 10) ? $"0{number}" : $"{number}";
        }
    }
}
