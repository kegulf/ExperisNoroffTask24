﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExperisNoroffTask24.Models {
    public class Supervisor {



        [Range(0, int.MaxValue, ErrorMessage = "Id must be a positive integer or 0")]
        [RegularExpression("^\\d+$", ErrorMessage = "Enter a valid number for Id")]
        public int? Id { get; set; } = -1;

        [Required(ErrorMessage = "Firstname is required")]
        public string Firstname { get; set; } = "No firstname";

        [Required(ErrorMessage = "Lastname is required")]
        public string Lastname { get; set; } = "No lastname";

        [Required(ErrorMessage = "Please specify availability")]
        public bool? IsAvailable { get; set; } = false;



        public string FullName { get => $"{Firstname} {Lastname}"; }

        public static List<Supervisor> GetSupervisorSet() => 
            new List<Supervisor> {
                new Supervisor() {
                    Id = 1, Firstname = "Speter", Lastname = "Parker", IsAvailable = true
                },
                new Supervisor() {
                    Id = 2, Firstname = "Greg", Lastname = "Linklater", IsAvailable = false
                },
                new Supervisor() {
                    Id = 3, Firstname = "Dean", Lastname = "von Schoultz", IsAvailable = true
                },
                new Supervisor() {
                    Id = 4, Firstname = "Sodd Martin", Lastname = "Hansen", IsAvailable = false
                },
                null,
                new Supervisor()

            };

        public override string ToString() {
            return FullName;
        }

    }
}
